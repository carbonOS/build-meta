#!/bin/sh
if [ "$USEPEERDNS" = "1" -a -f /etc/ppp/resolv.conf ]; then
    /usr/bin/resolvconf -a ${IFNAME} </etc/ppp/resolv.conf
fi
